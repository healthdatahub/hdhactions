#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from dataclasses import dataclass, field

import pandas as pd
from sqlalchemy import create_engine, text
from sqlalchemy.engine import Dialect
from sqlalchemy_utils import create_database, database_exists


@dataclass
class DBConn:

    dbtype: str
    dbconfig: dict

    def switch(self):
        return getattr(self, self.dbtype)()

    def postgres(self):
        return "postgresql://{user}:{password}@{host}:{port}/{database}".format(
            **self.dbconfig
        )


@dataclass
class DB(DBConn):

    db_conn: str = field(init=False)
    engine: Dialect = field(init=False)
    create_db: bool = True

    def __post_init__(self):
        self.db_conn = self.switch()
        try:
            self.engine = create_engine(self.db_conn)
            if self.engine:
                print(f"{self.engine} created..")
            if not database_exists(self.engine.url) and self.create_db:
                create_database(self.engine.url)
        except Exception as e:
            print(e)

    def transac_query(self, _query: str) -> object:
        try:
            with self.engine.connect() as conn:
                conn.execute("commit")
                info_result = conn.execute(text(_query))
                for info in info_result:
                    print(info)
        except Exception as e:
            print(e)

    def query(self, _query: str) -> pd.DataFrame:
        try:
            return pd.read_sql(_query, self.engine.connect())
        except Exception as e:
            print(e)
