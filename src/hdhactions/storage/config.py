#! /usr/bin/env python
# -*- coding: utf-8 -*-

import adlfs
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient


class Config:
    """
    Configuration of the AzureStorage wrapper module

    :param account_name: Name of the account name who want to connect to
    :type account_name: str

    :return: A BlobServiceClient
    :rtype: BlobServiceClient

    """

    def __init__(self, account_name):
        self.account_name = account_name
        self.fs = self.get_file_system()
        self.blob_service_client = self.get_service_client()

    def get_credential(self) -> DefaultAzureCredential:
        return DefaultAzureCredential(
            exclude_environment_credential=True,
            exclude_shared_token_cache_credential=True,
        )

    def get_service_client(self) -> BlobServiceClient:
        """
        Create blob service client using account name and key given in config file.
        """

        blob_service_client = BlobServiceClient(
            account_url=f"https://{self.account_name}.blob.core.windows.net",
            credential=self.get_credential(),
        )
        return blob_service_client

    def get_file_system(self):
        """
        Create a file system using account name and key given in config file.
        """

        fs = adlfs.AzureBlobFileSystem(
            account_name=self.account_name, credential=DefaultAzureCredential()
        )
        return fs
