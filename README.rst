.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://img.shields.io/coveralls/github/<USER>/hdhactions/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/hdhactions
    .. image:: https://img.shields.io/pypi/v/hdhactions.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/hdhactions/
    .. image:: https://img.shields.io/conda/vn/conda-forge/hdhactions.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/hdhactions
    .. image:: https://pepy.tech/badge/hdhactions/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/hdhactions
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/hdhactions

    .. image:: https://img.shields.io/gitlab/issues/healthdatahub/hdhactions/
        :alt: Gitlab issues

    .. image:: https://img.shields.io/gitlab/coverage/healthdatahub/hdhactions/refacto
        :alt: Gitlab code coverage

    .. image:: https://img.shields.io/badge/space-platform-yellow
       :alt: HDH's library for handling files in azure

.. image:: https://img.shields.io/gitlab/pipeline-status/healthdatahub/hdhactions?branch=refacto
    :alt: Gitlab pipeline status
    :target: https://gitlab.com/healthdatahub/hdhactions/-/pipelines

.. image:: https://img.shields.io/gitlab/coverage/healthdatahub/hdhactions/refacto
    :alt: Gitlab code coverage

.. image:: https://img.shields.io/badge/context-hdh-blue
    :alt: HDH's project called hdactions|




==========
hdhactions
==========


This package is an interface between the HDH Platform and Services (Azure Storage Blob, Databases, etc..) .

It consists of these subpackages:

- `Storage <./src/hdhactions/storage>`_  : Interface between the Platform and Azure Storage Blob
- `Connect <./src/hdhactions/connect>`_  : Interface between the Platform and Databases

Installation
=============

You can install the package directly from gitlab::

  pip install git+https://gitlab.com/healthdatahub/hdhactions.git


Tutorial
=========

All the commands given here are to be typed sequentially in the Jupyter notebook on the platform.

Step 1: check the input storage account
---------------------------------------

You will need the name of the storage account; ask your HDH contact if you do not have it.

.. code-block:: python

      from hdhactions.storage import AzureStorage
      input_account_name = "input_account_name_goes_here"
      azs_in = AzureStorage(account_name=input_account_name)
      print(azs_in.list_directories())

If an error occurs on the first command, the hdhactions module may not be available in the environment. Report this to your HDH contact.

If the account name was incorrect, the error will occur on the fourth line (rather than the third, because the storage is a `lazy <https://en.wikipedia.org/wiki/Lazy_loading>`_ connection object): the kernel will attempt multiple connection attempts before failing after roughly 20s.

The fourth line will list the available containers in the account storage. You should see at least one container for the input files.

For the rest of the tutorial, we assume the standard name ``data`` is used:

.. code-block:: python

       input_dir_name = "data"


Step 2: list files in the input container
------------------------------------------

.. code-block:: python

     flist = azs_in.list_files(directory=input_dir_name)
     print(len(flist), "file(s) in container")
     for f in flist: print(f)


Step 3: check available resources and file sizes
------------------------------------------------

To choose the appropriate method to interact with a file, you should first check your available memory...

.. code-block:: bash

      !free -mh

...and disk space...

.. code-block:: bash

      !df -h | grep -e "Filesystem" -e "/home/jovyan"

Then, check the size of your files (especially the biggest ones, typically ``ER_PRS_F`` tables for SNDS data)

.. code-block:: python

      files_to_check = flist  # or trim down the list to only check the biggest
      sizelist = []
      for filename in files_to_check:
          fileprops = azs_in.get_properties(directory=input_dir_name, files=filename)
          size_MB = fileprops["size"]/10**6
          sizelist.append((size_MB, filename))

      # List by increasing size
      for filename, size in sorted(sizelist):
          print("{} has size {:2.1f} MB".format(filename, size))


Step 4: interact with the files
--------------------------------

There are three methods to interact with the files, depending on their size. It is recommended to use the first (simplest) method that is available for your case.

Load in-memory
**************

This method requires that you have enough memory to fit the largest file (plus any possible processing outputs). Note that if the files are stored in a compressed format on the Azure storage, the decompressed size will be greater.

Most common file formats are handled by the method ``azs_in.read_file()``. For instance, csv files can be read as `pandas dataframes <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.htm>`_.

.. code-block:: python

  files_to_process = flist  # or use only a subset
  for filename in files_to_process:
      df = azs_in.read_file(
          directory = input_dir_name,
          filename = filename,
          zip = True, # assuming the file is zipped on Azure
          fmt = "csv",  # supported: "txt", "csv", "json", "hdf5", "pkl"
          sep = ";",  # column delimiter (see doc for pandas.read_csv)
          encoding = "latin1",  # file encoding (see doc for pandas.read_csv)
          )   # you can add any argument accepted by pandas.read_csv *except* chunksize
        # do something to process df


Download on-disk
****************

This method requires that you have enough disk space to fit the largest file, and that you can somehow process the file from the disk.


.. code-block:: python

  import os

  tmp_dir = "temp"
  if not os.path.exists(tmp_dir):
    os.mkdir(tmp_dir)

  files_to_process = flist  # or use only a subset
  for filename in files_to_process:
      try:
          azs_in.download_file(
              directory = input_dir_name,
              filename = filename,
              local_dir = tmp_dir,
              )
          # do something to process the file, located at {tmp_dir}/{filename}
      finally:  # ensure deletion of the on-disk file, even if there is an error during processing
          path = os.path.join(tmp_dir, filename)
          if os.path.exists(path):
              os.remove(path)



Stream the file
***************

That method is always available, but requires to be able to process a stream of the file. See the documentation of `azure.storage.blob.blobclient.download_blob() <https://docs.microsoft.com/en-us/python/api/azure-storage-blob/azure.storage.blob.blobclient?view=azure-python#azure-storage-blob-blobclient-download-blob>`_ for how to interact with the result.

.. code-block:: python

  blob_stream = azs.read_file_as_stream(directory=input_dir_name, filename="filename_goes_here")

Internally, the HDH team has a streaming script for csv files, which returns the file by dataframe chunks (format similar to ``pandas.read_csv(..., chunksize=N)``). That script can be shared on request. It will be merged to the hdhactions repository at a future date, but does not have the required maturity yet.



Step 5: push files to the output storage
-----------------------------------------

We assume that you have created files that you wish to push to the next step in the pipeline. Those files could be the same as in the input, or entirely new ones. We assume those files reside on the disk.

You will need the name of the output Azure storage (ask it to your HDH contact) as well as the container name, which can be determined in a very similar manner as in step 1:


.. code-block:: python

  output_account_name = "output_account_name_goes_here"
  azs_out = AzureStorage(account_name=output_account_name)
  print(azs_out.list_directories())

  output_dir_name = "data"

With that information, you can push the files:

.. code-block:: python

  azs_out.push_file(
      directory = output_dir_name,
      local_filename="path/to/file/on/disk",
      ) # remote_filename can be changed if necessary, see doc

Making Changes & Contributing
=============================

For more details on contributing tp this repository, see the `contributing guide <./CONTRIBUTING.rst>`_
