#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Regression test for https://gitlab.com/hdh-internal/data-management/hdhactions/-/issues/22

from io import BytesIO

import pytest

_FILES = [
    "a.test",
    "didi/b.test",
    "didi/c.test",
]


@pytest.fixture()
def setup_multifiles(storageTest):
    # Setup directories for unit testing

    dirname = "unittest"
    datastream = BytesIO(b"test")
    storageTest.reinitialize_container(dirname)
    for fname in _FILES:
        datastream.seek(0)
        storageTest.push_file_from_stream(
            stream=datastream, directory=dirname, remote_filename=fname, overwrite=True
        )

    yield dirname


def test_list_files(setup_multifiles, storageTest):
    """List files in container, in flat fashion."""

    dirname = setup_multifiles

    # set to tags1 and check with a get
    flist = storageTest.list_files(dirname)
    assert flist == sorted(_FILES)
