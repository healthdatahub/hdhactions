#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging

import adlfs
import pytest
from azure.storage.blob import BlobServiceClient

from hdhactions.storage import AzureStorage
from hdhactions.storage.azurestorage import HdhactionsAlreadyExistsError

logger = logging.getLogger(__name__)


class AzureStorageTest(AzureStorage):
    def __init__(self):
        super().__init__(account_name=pytest.account_name)

        logger.debug(f"Instanciating AzureStorageTest '{pytest.account_name}'.")

        # wipe existing containers and files
        bsc = self.blob_service_client
        for container in bsc.list_containers():
            bsc.delete_container(container)
            logger.debug(
                f"Removing container: {pytest.account_name}:{container['name']}."
            )
        # for container_name in self.list_directories():
        #     self.delete_directory(container_name)

    def get_service_client(self) -> BlobServiceClient:
        """Simulate a service client to azurite"""
        return BlobServiceClient(
            account_url=f"{pytest.azurite_host}/{self.account_name}",
            credential={
                "account_name": self.account_name,
                "account_key": pytest.storage_key,
            },
        )

    def get_file_system(self):
        return adlfs.AzureBlobFileSystem(connection_string=pytest.connection_string)

    def reinitialize_container(self, container_name: str):
        logger.info(f"Reinitializing '{container_name}'.")
        try:
            self.create_directory(container_name)
            logger.debug(f"Created '{container_name}' from scratch.")
            logger.exception("(test) here's the current stacktrace.")

        except HdhactionsAlreadyExistsError:
            cc = self._safe_get_container(container_name)
            cc.delete_container()
            self.create_directory(container_name)
            logger.debug(f"Deleted and recreated {container_name}.")


class AzureStorageTestParams(AzureStorage):
    def __init__(self, account_name):
        # self.account_name = account_name
        super().__init__(account_name=account_name)

    def get_service_client(self) -> BlobServiceClient:
        """Simulate a service client to azurite"""
        return BlobServiceClient(
            account_url=f"{pytest.azurite_host}/{self.account_name}",
            credential={
                "account_name": self.account_name,
                "account_key": pytest.storage_key,
            },
        )

    def get_file_system(self):
        connection_string = f"DefaultEndpointsProtocol=http;AccountName={self.account_name}\
            ;BlobEndpoint={pytest.azurite_host}/{pytest.account_name};"
        return adlfs.AzureBlobFileSystem(connection_string=connection_string)
