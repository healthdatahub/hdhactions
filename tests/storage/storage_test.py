#! /usr/bin/env python
# -*- coding: utf-8 -*-
# import os


import pytest

from .conf import AzureStorageTestParams


@pytest.fixture()
def setup(storageTest):
    # Setup directories for unit testing
    yield storageTest.create_directory(directory="unittest")


def test_list_directories(setup, storageTest):
    # print("##################", storageTest.fs)
    # print("@@@@@@@@@@@@@@@@@@",storageTest.fs.ls("unittest") )
    assert isinstance(storageTest.list_directories(), list)
    assert len(storageTest.list_directories()) != 0
    assert "unittest" in storageTest.list_directories()


@pytest.mark.parametrize("account_name", ["kjijdnaznjzaeyszq"])
def test_eager_loading(account_name):
    with pytest.raises(Exception):
        AzureStorageTestParams(account_name=account_name)
