=========
Changelog
=========

Version 1.6 (2023-10-10)
===========

- Added `get_credential` method that is accessible from the user. It also gives a better DefaultAzureCredential (with the option `exclude_shared_token_cache_credential` set), which should fix some nasty cross-storage transfer bugs.
- Added `dezip` function in utils
